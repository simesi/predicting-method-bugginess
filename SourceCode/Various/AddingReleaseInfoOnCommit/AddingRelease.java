package src;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



public class AddingRelease {

	private static final String dir= "Input";
	private static final String VERSIONS = "versions";
	private static final String FORMAT_DATE= "yyyy-MM-dd";
	private static final String HARD_DRIVE_NAME="E:";
	private static final String RELEASE_DATE="releaseDate";
	private static List<String> filesPath;
	private static final String SLASH="\\";
	public static Map<LocalDateTime, String> releaseNames;
	public static Map<LocalDateTime, String> releaseID;
	public static Map<String, Integer> commitsOfTestRelease;
	public static List<LocalDateTime> releases;
	private static Map<String,LocalDateTime> fromReleaseIndexToDate;
	private static String project;
	private static int releaseUnderExam=0;
	private static boolean serchingForCommitOfRelease = false;



	//questo programma associa un numero di release ad ogni 
	//commit nella directory "Input" avvenuto nel periodo di tempo in esame nel test set (eliminando gli altri commit) 
	public static void main(String[] args) {

		String row;
		String methodAndVersion;
		String method;
		String version;


		final File folder = new File(dir);
		filesPath = new ArrayList<>();
		//populate filesPath array with the paths of the files in Input folder
		listFiles(folder);
		//nella cartella Input devono essere inseriti i file di Daniel 'allcommits'


		//String folderRes = "Results";	        
		//Path path = Paths.get(new File("").getAbsolutePath()+SLASH+folderRes);
		try {
			//Files.createDirectories(path);		

			for (String file : filesPath) {

				//clone project
				project= file.substring(0, file.indexOf("_"));
				//System.out.println(project);
				//gitClone(project);

				findNumberOfReleases(project);

				//comando git per ottenere date dei commit nelle release di test
				SearchForCommitsOfGivenRelease(Integer.min(Integer.max(Math.floorDiv(fromReleaseIndexToDate.size(),10),3),5));
				serchingForCommitOfRelease=false;



				//--------------------------------------//

				//open Daniel commit-method file
				BufferedReader csvReader = new BufferedReader(new FileReader(dir+SLASH+file));

				//Name of CSV for output
				String outname = file.replace(".csv", "_clean.csv");

				FileWriter fileWriter = new FileWriter(outname);

				fileWriter.append("Project;Commit;Method");
				fileWriter.append("\n");

				//discard the header
				csvReader.readLine();
				String commit;
				while ((row = csvReader.readLine()) != null) {

					String[] entry = row.split(",");
					commit=entry[1]; //leggo l'id del commit

					if (commitsOfTestRelease.containsKey(commit.trim())) {


						//scrivo su un altro file i commit selezionati
						fileWriter.append(project);
						fileWriter.append(";");
						fileWriter.append(commit+"-"+commitsOfTestRelease.get(commit.trim()));
						fileWriter.append(";");
						entry[2]= entry[2].trim().replaceAll(";", ",");
						fileWriter.append(entry[2].substring(0,entry[2].length() - 1)); //discard of  last ","
						fileWriter.append("\n");
					}


				}
				csvReader.close();
				fileWriter.close();
				commitsOfTestRelease.clear();

			}
		}

		catch (IOException |/* InterruptedException |*/ JSONException e) {
			e.printStackTrace();
			System.exit(-1);	
		}
	}


	private static class StreamGobbler extends Thread {

		private final InputStream is;


		private StreamGobbler(InputStream is) {

			this.is = is;

		}

		@Override

		public void run() {

			try (BufferedReader br = new BufferedReader(new InputStreamReader(is));) {

				String line;



				while ((line = br.readLine()) != null) {

					if (serchingForCommitOfRelease) {
						getCommits(line,br);
					}
					else {
						//System.out.println(line);
					}
				}


			} catch (IOException ioe) {

				ioe.printStackTrace();
				System.exit(-1);
			}

		}

		private void getCommits(String line, BufferedReader br) {

			String nextLine;
			String myCommit;			


			line=line.trim();
			String[] tokens = line.split("\\s+");
			myCommit= tokens[0];
			commitsOfTestRelease.put(myCommit, releaseUnderExam);
			try {

				// otteniamo tutti i commit avvenuti nel periodo del test set 
				nextLine =br.readLine();

				//ora prendo i commit
				while(nextLine != null) {
					nextLine=nextLine.trim();
					tokens = nextLine.split("\\s+");
					myCommit= tokens[0];
					commitsOfTestRelease.put(myCommit, releaseUnderExam);
					nextLine =br.readLine();
				}

			} catch (IOException e) {
				e.printStackTrace();
				System.exit(-1);
			} 
		}
	}


	private static void listFiles(final File folder) {
		for (final File fileEntry : folder.listFiles()) {

			//System.out.println(fileEntry.getName()); //OPENJPA_Commit.csv
			//System.out.println(fileEntry.getAbsolutePath()); //E:\Programmi\Eclipse\collectDataForRQ1\Results\OPENJPA_Commit.csv

			filesPath.add(fileEntry.getName());
		}
	}

	//questo metodo fa il 'git clone' della repository (necessario per poter ricavare successivamente il log dei commit)   
	private static void gitClone(String project) throws IOException, InterruptedException {

		Path directory;
		String originUrl = "https://github.com/apache/"+project+".git";


		directory = Paths.get(new File("").getAbsolutePath()+SLASH+project);

		runCommand(directory.getParent(), "git", "clone", originUrl, directory.getFileName().toString());

	}

	public static void runCommand(Path directory, String... command) throws IOException, InterruptedException {

		Objects.requireNonNull(directory, "directory � NULL");

		if (!Files.exists(directory)) {

			throw new SecurityException("can't run command in non-existing directory '" + directory + "'");

		}

		ProcessBuilder pb = new ProcessBuilder()

				.command(command)

				.directory(directory.toFile());


		runProcAndWait(pb);

	}

	private static void runProcAndWait(ProcessBuilder pb) throws IOException, InterruptedException {
		//lancio un nuovo processo che invocher� il comando 'command',
		//nella working directory fornita. 
		Process p = pb.start();

		StreamGobbler errorGobbler = new StreamGobbler(p.getErrorStream());

		StreamGobbler outputGobbler = new StreamGobbler(p.getInputStream());

		outputGobbler.start();

		errorGobbler.start();

		int exit = p.waitFor();

		errorGobbler.join();

		outputGobbler.join();

		if (exit != 0) {

			throw new AssertionError(String.format("runCommand returned %d", exit));

		}
	}

	//questo metodo lancia un comando che ritorner� tutti gli id dei commit avvenuti nella/e release di test
	private static void SearchForCommitsOfGivenRelease(int release) {

		//directory da cui far partire il comando git    
		Path directory = Paths.get(new File("").getAbsolutePath()+SLASH+project);
		String command;

		commitsOfTestRelease= new HashMap<>();

		//setting a flag
		serchingForCommitOfRelease=true;
		try {

			//pi� release di test 
			if (release==4||release==5) {

				command = "git log --pretty=format:\"%H\""
						+ " --since="+fromReleaseIndexToDate.get(String.valueOf(release-2))+
						" --until="+fromReleaseIndexToDate.get(String.valueOf(release-1))+" -- *.java" ;	
				releaseUnderExam=release-1;

				runCommandOnShell(directory, command);
			}

			command = "git log --pretty=format:\"%H\""
					+ " --since="+fromReleaseIndexToDate.get(String.valueOf(release-1))+
					" --until="+fromReleaseIndexToDate.get(String.valueOf(release))+" -- *.java" ;	
			releaseUnderExam=release;
			runCommandOnShell(directory, command);




		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		} catch (InterruptedException e) {
			e.printStackTrace();
			Thread.currentThread().interrupt();
		}
	}

	public static void runCommandOnShell(Path directory, String command) throws IOException, InterruptedException {
		ProcessBuilder pb = new ProcessBuilder("cmd.exe", "/c",HARD_DRIVE_NAME+" && cd "+directory.toString()+" && "+command);	

		runProcAndWait(pb);

	}


	private static void findNumberOfReleases(String project) throws JSONException, IOException {


		// qui si calcolano le release
		Integer i = 0;
		JSONObject json ;

		//Fills the arraylist with releases dates and orders them
		//Ignores releases with missing dates
		releases = new ArrayList<>();

		String url = "https://issues.apache.org/jira/rest/api/2/project/"+ project.toUpperCase();
		json = readJsonFromUrl(url);
		JSONArray versions = json.getJSONArray(VERSIONS);
		releaseNames = new HashMap<>();
		releaseID = new HashMap<> ();
		for (i = 0; i < versions.length(); i++ ) {
			String name = "";
			String id = "";
			if(versions.getJSONObject(i).has(RELEASE_DATE)) {
				if (versions.getJSONObject(i).has("name"))
					name = versions.getJSONObject(i).get("name").toString();
				if (versions.getJSONObject(i).has("id"))
					id = versions.getJSONObject(i).get("id").toString();
				addRelease(versions.getJSONObject(i).get(RELEASE_DATE).toString(),
						name,id);
			}
		}


		Comparator <LocalDateTime> comp = (o1,o2)->o1.compareTo(o2);
		// order releases by date
		Collections.sort(releases, comp);




		//--------------------------------------------------------
		fromReleaseIndexToDate=new HashMap<>();
		//popolo un'HasMap con associazione indice di release-data delle release
		for ( i = 1; i <= releases.size(); i++) {
			fromReleaseIndexToDate.put(i.toString(),releases.get(i-1));
			//System.out.println("Release "+i+" si chiama: "+releaseNames.get(releases.get(i-1))+" "+fromReleaseIndexToDate.get(i.toString()));

		}



		//cancellazione preventiva della directory clonata del progetto (se esiste)   
		//recursiveDelete(new File(new File("").getAbsolutePath()+SLASH+projectName));


	}

	public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
		JSONObject json;
		InputStream is = new URL(url).openStream();
		try(BufferedReader rd = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8))) {
			String jsonText = readAll(rd);
			json = new JSONObject(jsonText);

		} finally {
			is.close();
		}
		return json;
	}

	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}

	public static void addRelease(String strDate, String name, String id) {
		LocalDate date = LocalDate.parse(strDate);
		LocalDateTime dateTime = date.atStartOfDay();
		if (!releases.contains(dateTime))
			releases.add(dateTime);
		releaseNames.put(dateTime, name);
		releaseID.put(dateTime, id);
	}
}
