package myPackage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvException;


public class FromOutputDanielToCommit {


	// qui partendo dal file output di Daniel si ottengono i commits buggy, mi calcolo le release e 	
	// poi modifico il file dei risultati dele metriche di commit


	private static String projectName="OPENJPA";
	private static String projectNameGit="apache/openjpa.git";
	private static final int numOfThreads= 5; //set this variable to set the number of threads


	//this array has much element as threads and every cell is filled with the value of the version of the given commit
	private static Integer[] threadsCommitVersion= new Integer[numOfThreads];
	///////////////////////////////    used to set boolean conditions for every thread
	private static Boolean startThreadsOperations = false;////////////////////////////////
	/////////////////////////////////



	private static final String outputCsvString = projectName+"_Commit.csv"; //file con le metriche di metodo da me prima calcolato
	private static final String inputFile= projectName.toLowerCase()+"-output.csv"; //file dato da Daniel

	//------------------------------------------------------------------------
	private static final String ECHO = "echo "; 
	private static final String VERSIONS = "versions";
	private static final String FIELDS ="fields";
	private static final String FORMATNUMSTAT= " --format= --numstat -- ";
	private static final String URLJIRA="https://issues.apache.org/jira/rest/api/2/search?jql=project=%22";
	private static final String PIECE_OF_URL_JIRA="%22AND%22issueType%22=%22Bug%22AND(%22status%22=%22closed%22OR";
	private static final String SLASH="\\";
	private static final String MAX_RESULT="&maxResults=";
	private static final String ISSUES= "issues";
	private static final String TOTAL= "total"; 
	private static final String FORMAT_DATE= "yyyy-MM-dd";
	private static final String RELEASE_DATE="releaseDate";
	public static Map<LocalDateTime, String> releaseID;
	public static Map<LocalDateTime, String> releaseNames;
	private static Map<String,LocalDateTime> fromReleaseIndexToDate=new HashMap<>();


	public static List<LocalDateTime> releases;
	private static List<String[]> csvBody;



	public static void main(String[] args) { 


		//cancellazione preventiva della directory clonata del progetto (se esiste)   
		//recursiveDelete(new File(new File("").getAbsolutePath()+SLASH+projectName));

		String row = "";

		File csvFile = new File(inputFile);
		if (!csvFile.isFile()) {
			System.out.println("No input file founded");
			System.exit(-1);
		}

		findNumberOfReleases();

		/*try {
			//si fa il clone della versione odierna del progetto
			gitClone();	
		}
		catch (InterruptedException | IOException e) {
			e.printStackTrace();
			Thread.currentThread().interrupt();
			System.exit(-1);
		}	*/

		//ora si scandisce il file da me creato con le metriche e si setta bug = "yes"				
		CSVParser parser = new CSVParserBuilder().withSeparator(';').build();

		File outputCsv = new File(outputCsvString);

		try { 
			// Read existing file		
			CSVReader reader= new CSVReaderBuilder(new FileReader(outputCsv)).withCSVParser(parser).build();
			csvBody = reader.readAll();
			reader.close();
			//each String[] representing a line of the file




			startThreadsOperations = true;
			//////////////////////////////////////////////
			for ( int i = 0; i <numOfThreads; i++)
			{
				threadsCommitVersion[i]= 0;				
			}



			ArrayList<Thread> threads= new ArrayList<Thread>();

			for(int i = 0; i < numOfThreads; i++) {

				Thread t = new Thread(new SimpleCommitAnalyzerRunner(numOfThreads,i));
				t.start();
				threads.add(t);
			}



			for(int i = 0; i < numOfThreads; i++) {
				threads.get(i).join();
			}

			//ora che tutti i threads hanno finito, riscriviamo il file con le metriche 

			// Write to CSV file which is open
			CSVWriter writer;
			try {
				writer = new CSVWriter(new FileWriter(outputCsv),';',
						CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.NO_ESCAPE_CHARACTER,
						CSVWriter.DEFAULT_LINE_END);

				writer.writeAll(csvBody);
				csvBody.clear();
				writer.flush();
				writer.close();

			} catch (IOException e) {
				e.printStackTrace();
				System.exit(-1);
			}


		} 
		catch (InterruptedException exc) {
			exc.printStackTrace();
			Thread.currentThread().interrupt();
			System.exit(-1);
		} catch (IOException e) {
			e.printStackTrace();
			Thread.currentThread().interrupt();
			System.exit(-1);
		} catch (CsvException e) {			
			e.printStackTrace();
			Thread.currentThread().interrupt();
			System.exit(-1);
		}


		//System.out.println("Tutit i dati di Daniel sono stati analizzati e scritti");
	}




	private static void findNumberOfReleases()  {

		try {


			// qui si calcolano le release
			Integer i = 0;
			JSONObject json ;

			//Fills the arraylist with releases dates and orders them
			//Ignores releases with missing dates
			releases = new ArrayList<>();

			String url = "https://issues.apache.org/jira/rest/api/2/project/" + projectName;
			json = readJsonFromUrl(url);
			JSONArray versions = json.getJSONArray(VERSIONS);
			releaseNames = new HashMap<>();
			releaseID = new HashMap<> ();
			for (i = 0; i < versions.length(); i++ ) {
				String name = "";
				String id = "";
				if(versions.getJSONObject(i).has(RELEASE_DATE)) {
					if (versions.getJSONObject(i).has("name"))
						name = versions.getJSONObject(i).get("name").toString();
					if (versions.getJSONObject(i).has("id"))
						id = versions.getJSONObject(i).get("id").toString();
					addRelease(versions.getJSONObject(i).get(RELEASE_DATE).toString(),
							name,id);
				}
			}


			Comparator <LocalDateTime> comp = (o1,o2)->o1.compareTo(o2);
			// order releases by date
			Collections.sort(releases, comp);




			//--------------------------------------------------------

			//popolo un'HasMap con associazione indice di release-data delle release
			for ( i = 1; i <= releases.size(); i++) {
				fromReleaseIndexToDate.put(i.toString(),releases.get(i-1));
				//System.out.println("Release "+i+" si chiama: "+releaseNames.get(releases.get(i-1))+
						//" );
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}


	public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
		JSONObject json;
		InputStream is = new URL(url).openStream();
		try(BufferedReader rd = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8))) {
			String jsonText = readAll(rd);
			json = new JSONObject(jsonText);

		} finally {
			is.close();
		}
		return json;
	}
	/*
	  Java isn't able to delete folders with data in it. We have to delete
	     all files before deleting the directory.This utility class is used to delete 
	  folders recursively in java.*/

	public static void recursiveDelete(File file) {
		//to end the recursive loop
		if (!file.exists())
			return;

		//if directory exists, go inside and call recursively
		if (file.isDirectory()) {
			for (File f : file.listFiles()) {
				//call recursively
				recursiveDelete(f);
			}
		}
		//call delete to delete files and empty directory
		try {
			//disabling Read Only property of the file to be deleted resolves the issue triggered by Files.delete
			Files.setAttribute(file.toPath(), "dos:readonly", false);
			Files.deleteIfExists(file.toPath());
		} catch (IOException| InvalidPathException e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	public static void addRelease(String strDate, String name, String id) {
		LocalDate date = LocalDate.parse(strDate);
		LocalDateTime dateTime = date.atStartOfDay();
		if (!releases.contains(dateTime))
			releases.add(dateTime);
		releaseNames.put(dateTime, name);
		releaseID.put(dateTime, id);
	}

	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}


	//questo metodo fa il 'git clone' della repository (necessario per poter ricavare successivamente il log dei commit)   
	private static void gitClone() throws IOException, InterruptedException {

		Path directory;
		String originUrl = "https://github.com/"+projectNameGit;


		directory = Paths.get(new File("").getAbsolutePath()+SLASH+projectName);

		runCommand(directory.getParent(), "git", "clone", originUrl, directory.getFileName().toString());

	}


	public static void runCommand(Path directory, String... command) throws IOException, InterruptedException {

		Objects.requireNonNull(directory, "directory � NULL");

		if (!Files.exists(directory)) {

			throw new SecurityException("can't run command in non-existing directory '" + directory + "'");

		}

		ProcessBuilder pb = new ProcessBuilder()

				.command(command)

				.directory(directory.toFile());


		runProcAndWait(pb);

	}

	private static void runProcAndWait(ProcessBuilder pb) throws IOException, InterruptedException {
		//lancio un nuovo processo che invocher� il comando 'command',
		//nella working directory fornita. 
		Process p = pb.start();

		StreamGobbler errorGobbler = new StreamGobbler(p.getErrorStream());

		StreamGobbler outputGobbler = new StreamGobbler(p.getInputStream());

		outputGobbler.start();

		errorGobbler.start();

		int exit = p.waitFor();

		errorGobbler.join();

		outputGobbler.join();

		if (exit != 0) {

			throw new AssertionError(String.format("runCommand returned %d", exit));

		}
	}

	private static void runProcAndWaitWithTid(ProcessBuilder pb, int tid) throws IOException, InterruptedException {
		//lancio un nuovo processo che invocher� il comando 'command',
		//nella working directory fornita. 
		Process p = pb.start();

		StreamGobbler errorGobbler = new StreamGobbler(p.getErrorStream(), tid);

		StreamGobbler outputGobbler = new StreamGobbler(p.getInputStream(), tid);

		outputGobbler.start();

		errorGobbler.start();

		int exit = p.waitFor();

		errorGobbler.join();

		outputGobbler.join();

		if (exit != 0) {

			throw new AssertionError(String.format("runCommand returned %d", exit));

		}
	}
	private static class SimpleCommitAnalyzerRunner implements Runnable { 

		private int start; 
		private int stride;

		public SimpleCommitAnalyzerRunner(int stride, int start) {
			this.stride = stride;
			this.start = start;
		}

		public void run() {

			analyzeCommit(start,stride);

		}
	}

	private static class StreamGobbler extends Thread {

		private final InputStream is;
		private int tid=100; //don't care value

		private StreamGobbler(InputStream is) {

			this.is = is;

		}

		private StreamGobbler(InputStream is, int tid) {

			this.is = is;
			this.tid = tid;

		}

		@Override

		public void run() {

			try (BufferedReader br = new BufferedReader(new InputStreamReader(is));) {

				String line;



				while ((line = br.readLine()) != null) {

					if(startThreadsOperations) {

						String nextLine;
						int age=0;
						int count=0;
						LocalDate DateCommit = null;

						line=line.trim();
						//"one or more whitespaces = \\s+"
						String[] tokens = line.split("\\s+");

						DateTimeFormatter format = DateTimeFormatter.ofPattern(FORMAT_DATE);

						//il primo output � la data del commit attuale ------------------------------
						DateCommit = LocalDate.parse(tokens[0],format);


						for(int release=1;release<=fromReleaseIndexToDate.size();release++) {

							if(DateCommit.atStartOfDay().isBefore(fromReleaseIndexToDate.get(String.valueOf(1)))) {
								threadsCommitVersion[tid]=1;
								break;
							}
							//abbiamo raggiunto nel for l'ultima release
							else if(release==fromReleaseIndexToDate.size()) {
								threadsCommitVersion[tid]=release;
								break;								

							}// fine if ultima release

							else if ((DateCommit.atStartOfDay().isAfter(fromReleaseIndexToDate.get(String.valueOf(release)))
									&&(DateCommit.atStartOfDay().isBefore(fromReleaseIndexToDate.get(String.valueOf(release+1)))||
											(DateCommit.atStartOfDay().isEqual(fromReleaseIndexToDate.get(String.valueOf(release+1))))))) {
								threadsCommitVersion[tid]= release+1;
								break;//per uscire dal for una volta trovata la Injected version
							}
						}

					}

					else {
						//System.out.println(line);
					}
				}

			} catch (IOException ioe) {

				ioe.printStackTrace();
				System.exit(-1);
			}

		}
	}



	//occorre calcolare la data dei commit (e quindi la release)
	private static void getDateOfCommit(String bugCommit, int tid) {

		//directory da cui far partire il comando git    
		Path directory = Paths.get(new File("").getAbsolutePath()+
				SLASH+projectName);
		String command;

		//si printa la data del commit passato e anche del commit precedente
		try {

			command = "git log --date=short -1  --format=%ad "+bugCommit;
			runCommandOnShellWithTid(directory, command,tid);




		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		} catch (InterruptedException e) {
			e.printStackTrace();
			Thread.currentThread().interrupt();
		}
	}

	public static void runCommandOnShellWithTid(Path directory, String command, int tid) throws IOException, InterruptedException {

		ProcessBuilder pb = new ProcessBuilder("cmd.exe", "/c","E: && cd "+directory.toString()+" && "+command);	

		runProcAndWaitWithTid(pb, tid);

	}
	public static void runCommandOnShell(Path directory, String command) throws IOException, InterruptedException {

		ProcessBuilder pb = new ProcessBuilder("cmd.exe", "/c","E: && cd "+directory.toString()+" && "+command);	

		runProcAndWait(pb);

	}
	//each thread with index 'start' analyze and set the field 'Buggy'
	public static void analyzeCommit(int start, int stride){

		try (
				BufferedReader csvReader = new BufferedReader(new FileReader(inputFile));
				)
		{

			//si legge l'header
			String row=csvReader.readLine();
			int n=0;


			while ((row = csvReader.readLine()) != null) {


				if ((n==start)||Math.floorMod(n, numOfThreads)==start) {

					//if (start==0) //solo il primo thread printa
					//	System.out.println("Lettura riga "+n+" file di Daniel del thread "+start);

					String[] entry = row.split(",");

					//2= Fix commit id, 4 = bug commit id, 5 = path-class.method

					String bugCommit= entry[4];


					//ora si fa una query git per ottenere la data del commit buggy
					getDateOfCommit(bugCommit,start);


					if (threadsCommitVersion[start]>
					Integer.min(Integer.max(Math.floorDiv(fromReleaseIndexToDate.size(),10),3),5)) {
						//System.out.println("Bug oltre le release considerate quindi scartato -----------");
						n++;
						continue;
					}

					for(int i=1; i<csvBody.size(); i++){ //for each row
						String[] columnArray = csvBody.get(i); //prendi l'insieme delle colonne della riga i

						// 2 = commit column
						if (columnArray[2].equals(bugCommit)) { //prendo il valore della colonna commit della riga i-esima
							csvBody.get(i)[18] = "YES"; //la 19� colonna viene impostata a buggy
							break;
						}
					}
				}
				n++;
			}//fine while


		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}







