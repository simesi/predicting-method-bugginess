# Predicting Method Bugginess
===============

The source code in the directory 'Various' works on csv files in order to generate results for the research questions explained in the article "Shall We Predict the Defectiveness of Methods or Classes?".

Installing
----------
First of all, please make sure that Java 11+ is installed in your environment.
Download the source code in https://gitlab.com/simesi/predicting-method-bugginess/-/tree/master/SourceCode/Various.

The following dependencies are needed:
- hyperPipes     (https://www.cs.waikato.ac.nz/ml/weka/packageMetaDataTemporary/hyperPipes/index.html)
- JSON   (https://jar-download.com/artifacts/org.json)
- simpleEducationalLearningSchemes   (https://www.cs.waikato.ac.nz/ml/weka/packageMetaDataTemporary/simpleEducationalLearningSchemes/index.html)
- SMOTE (https://www.cs.waikato.ac.nz/ml/weka/packageMetaDataTemporary/SMOTE/index.html)
- VotingFeatureintervals     (https://www.cs.waikato.ac.nz/ml/weka/packageMetaDataTemporary/votingFeatureIntervals/index.html)
- WEKA  (http://www.java2s.com/Code/Jar/w/Downloadwekajar.html)

In order to generate results for methods for RQ1, the project (https://github.com/kusumotolab/FinerGit) must be downloaded and builded on your local machine.

Usage
-----

The source code in the directory `Project` is the main program which is responsible for generating the results for the RQ.
In detail, 'Project\Main' has some global variables that need to be modified as needed. Some of these are:

`PATH_TO_FINER_GIT_JAR` concern the path of FinerGit framework
`studyMethodMetrics`, `studyClassMetrics` and `studyCommitMetrics` concern the granularity of RQ1 
`doResearchQuest1` concern if execute or not the code relating to RQ1

Note: the source code in order to generate the result of of RQ2 and RQ3 is executed only if `doResearchQuest2` is set to true.

The source code in the directories `CleaningCommit`,`CleaningMethods` and `CleaningClass` needs to be executed in order to filter the input csv file with the map between a fix commit and the bug-introducing commits and defective methods. then it labels the commits, methods or classes founded by the main code in the directory `Project`

The source code in the directory `AddingReleaseInfoOnCommit` needs to be executed in order to add a temporal dimension (i.e. number of release modified) to the commits.

```
